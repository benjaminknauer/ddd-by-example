CREATE TABLE account
(
    id       UUID    NOT NULL PRIMARY KEY,
    amount   INT     NOT NULL,
    currency VARCHAR NOT NULL
);
