CREATE TABLE transaction
(
    id       UUID    NOT NULL PRIMARY KEY,
    from_account   UUID    NOT NULL,
    to_account     UUID    NOT NULL,
    amount   INT     NOT NULL,
    currency VARCHAR NOT NULL,

    CONSTRAINT fk_transaction_from_account FOREIGN KEY (from_account) REFERENCES account (id),
    CONSTRAINT fk_transaction_to_account FOREIGN KEY (to_account) REFERENCES account (id)
);
