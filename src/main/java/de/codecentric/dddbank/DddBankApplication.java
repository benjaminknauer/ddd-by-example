package de.codecentric.dddbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DddBankApplication {

    public static void main(String[] args) {
        SpringApplication.run(DddBankApplication.class, args);
    }

}
