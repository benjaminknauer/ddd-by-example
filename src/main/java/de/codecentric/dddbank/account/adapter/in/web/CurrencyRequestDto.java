package de.codecentric.dddbank.account.adapter.in.web;

import de.codecentric.dddbank.account.domain.model.shared.Currency;

public enum CurrencyRequestDto {
    EUR, USD;

    Currency toDomain() {
        return switch (this) {
            case EUR -> Currency.EUR;
            case USD -> Currency.USD;
        };
    }
}
