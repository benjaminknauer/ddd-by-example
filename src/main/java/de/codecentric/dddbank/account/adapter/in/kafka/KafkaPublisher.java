package de.codecentric.dddbank.account.adapter.in.kafka;

import de.codecentric.dddbank.account.domain.model.transaction.TransactionCreated;
import de.codecentric.dddbank.account.application.port.out.PublishTransactionsPort;
import org.springframework.stereotype.Component;

@Component
public class KafkaPublisher implements PublishTransactionsPort {

    @Override
    public void publishTransactionCreated(TransactionCreated transactionCreated) {

        var event = KafkaTransactionEvent.of(transactionCreated.transaction());

        System.out.println("---");
        System.out.println("Publishing Transaction to Kafka:");
        System.out.println(event);
        System.out.println("---");

    }
}
