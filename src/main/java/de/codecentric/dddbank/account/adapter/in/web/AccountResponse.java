package de.codecentric.dddbank.account.adapter.in.web;

import de.codecentric.dddbank.account.domain.model.account.Account;
import de.codecentric.dddbank.account.domain.model.shared.Currency;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema
record AccountResponse(
        String iban,

        int balance,
        CurrencyResponse currency
) {

    static AccountResponse of(Account account) {
        return new AccountResponse(
                account.getId().id().toString(),
                account.getBalance().getAmount(),
                CurrencyResponse.of(account.getBalance().getCurrency())
        );
    }

}

enum CurrencyResponse {
    EUR, USD;

    static CurrencyResponse of(Currency currency) {
        return switch (currency) {
            case USD -> CurrencyResponse.USD;
            case EUR -> CurrencyResponse.EUR;
        };
    }

}
