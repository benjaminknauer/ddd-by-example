package de.codecentric.dddbank.account.adapter.in.web;

import de.codecentric.dddbank.account.application.port.in.CreateAccountUseCase;
import de.codecentric.dddbank.account.domain.model.account.Account;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.http.ResponseEntity;
import org.springframework.modulith.ApplicationModuleListener;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AccountController {

    private final CreateAccountUseCase createAccountUseCase;

    @PutMapping("/account")
    public ResponseEntity<AccountResponse> createAccount() {
        var account = createAccountUseCase.createNewAccount();
        var response = AccountResponse.of(account);

        return ResponseEntity.ok(response);
    }

}
