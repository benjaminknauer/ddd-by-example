package de.codecentric.dddbank.account.adapter.in.web;

import de.codecentric.dddbank.account.application.port.in.LoadTransactionsUseCase;
import de.codecentric.dddbank.account.application.port.in.SendMoneyUseCase;
import de.codecentric.dddbank.account.domain.model.account.Account;
import de.codecentric.dddbank.account.domain.model.shared.MonetaryAmount;
import de.codecentric.dddbank.account.domain.model.transaction.Transaction;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class TransactionController {

    private final SendMoneyUseCase sendMoneyUseCase;
    private final LoadTransactionsUseCase loadTransactionsUseCase;


    @GetMapping("/transactions")
    public ResponseEntity<List<Transaction>> getTransactions() {
        var transactions = loadTransactionsUseCase.loadAllTransactions();

        return ResponseEntity.ok(transactions);
    }

    @GetMapping("/account/{accountId}")
    public ResponseEntity<List<Transaction>> getTransactionsAssociatedWith(@PathVariable UUID accountId) {
        var transactions = loadTransactionsUseCase.loadAllAssociatedWith(new Account.AccountId(accountId));

        return ResponseEntity.ok(transactions);
    }

    @PostMapping("/send")
    public ResponseEntity<Transaction> sendMoney(@RequestBody SendMoneyRequest request) {
        var fromAccount = new Account.AccountId(request.fromAccountId());
        var toAccount = new Account.AccountId(request.toAccountId());
        var amount = MonetaryAmount.of(
                request.ammount(),
                request.currency().toDomain()
        );
        var transaction = sendMoneyUseCase.sendMoney(
                fromAccount,
                toAccount,
                amount
        );

        return ResponseEntity.ok(transaction);
    }

}
