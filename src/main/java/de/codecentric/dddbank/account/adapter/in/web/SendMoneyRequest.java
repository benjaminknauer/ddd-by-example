package de.codecentric.dddbank.account.adapter.in.web;

import java.util.UUID;

public record SendMoneyRequest(
        UUID fromAccountId,
        UUID toAccountId,
        int ammount,
        CurrencyRequestDto currency
) {
}

