package de.codecentric.dddbank.account.adapter.in.kafka;

import de.codecentric.dddbank.account.domain.model.transaction.Transaction;
import lombok.*;

@Value
@Builder(access = AccessLevel.PRIVATE)
@ToString
class KafkaTransactionEvent {
    @NonNull String id;
    @NonNull String fromAccountId;
    @NonNull String toAccountId;
    int amount;
    @NonNull String currency;


    public static KafkaTransactionEvent of(Transaction transaction) {
        return KafkaTransactionEvent.builder()
                .id(transaction.getId().id().toString())
                .fromAccountId(transaction.getFrom().getId().id().toString())
                .toAccountId(transaction.getTo().getId().id().toString())
                .amount(transaction.getAmount().getAmount())
                .currency(transaction.getAmount().getCurrency().toString())
                .build();
    }
}
