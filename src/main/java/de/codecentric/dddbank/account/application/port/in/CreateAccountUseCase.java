package de.codecentric.dddbank.account.application.port.in;

import de.codecentric.dddbank.account.domain.model.account.Account;

public interface CreateAccountUseCase {

    Account createNewAccount();

}
