package de.codecentric.dddbank.account.application.service;

import de.codecentric.dddbank.account.application.port.in.LoadTransactionsUseCase;
import de.codecentric.dddbank.account.application.port.in.SendMoneyUseCase;
import de.codecentric.dddbank.account.domain.model.account.Account;
import de.codecentric.dddbank.account.domain.model.account.AccountRepository;
import de.codecentric.dddbank.account.domain.model.shared.MonetaryAmount;
import de.codecentric.dddbank.account.domain.model.transaction.Transaction;
import de.codecentric.dddbank.account.domain.model.transaction.TransactionRepository;
import de.codecentric.dddbank.account.domain.service.SendMoneyDomainService;
import lombok.RequiredArgsConstructor;
import org.jmolecules.ddd.annotation.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionService implements LoadTransactionsUseCase, SendMoneyUseCase {

    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;
    private final SendMoneyDomainService sendMoneyDomainService;

    @Override
    public List<Transaction> loadAllTransactions() {
        return transactionRepository.findAll();
    }

    @Override
    public List<Transaction> loadAllFrom(Account.AccountId accountId) {
        return transactionRepository.findFrom(accountId);
    }

    @Override
    public List<Transaction> loadAllTo(Account.AccountId accountId) {
        return transactionRepository.findTo(accountId);
    }

    @Override
    public List<Transaction> loadAllAssociatedWith(Account.AccountId accountId) {
        return transactionRepository.findAssociated(accountId);
    }

    @Transactional
    @Override
    public Transaction sendMoney(Account.AccountId from, Account.AccountId to, MonetaryAmount amount) {
        var fromAccount = accountRepository.findById(from).orElseThrow();
        var toAccount = accountRepository.findById(to).orElseThrow();

        var transaction = sendMoneyDomainService.sendMoney(fromAccount, toAccount, amount);

        transactionRepository.save(transaction);
        accountRepository.save(fromAccount);
        accountRepository.save(toAccount);

        return transaction;
    }

}
