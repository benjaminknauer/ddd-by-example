package de.codecentric.dddbank.account.application.port.in;

import de.codecentric.dddbank.account.domain.model.account.Account;
import de.codecentric.dddbank.account.domain.model.transaction.Transaction;

import java.util.List;

public interface LoadTransactionsUseCase {

    List<Transaction> loadAllTransactions();

    List<Transaction> loadAllFrom(Account.AccountId accountId);

    List<Transaction> loadAllTo(Account.AccountId accountId);

    List<Transaction> loadAllAssociatedWith(Account.AccountId accountId);

}
