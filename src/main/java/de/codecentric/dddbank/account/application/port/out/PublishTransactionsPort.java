package de.codecentric.dddbank.account.application.port.out;

import de.codecentric.dddbank.account.domain.model.transaction.TransactionCreated;

public interface PublishTransactionsPort {

    void publishTransactionCreated(TransactionCreated transactionCreated);

}
