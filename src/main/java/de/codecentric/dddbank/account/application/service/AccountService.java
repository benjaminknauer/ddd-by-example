package de.codecentric.dddbank.account.application.service;

import de.codecentric.dddbank.account.application.port.in.CreateAccountUseCase;
import de.codecentric.dddbank.account.domain.model.account.Account;
import de.codecentric.dddbank.account.domain.model.account.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.jmolecules.ddd.annotation.Service;

@Service
@RequiredArgsConstructor
public class AccountService implements CreateAccountUseCase {

    private final AccountRepository accountRepository;

    @Override
    public Account createNewAccount() {
        var account = Account.create();
        accountRepository.save(account);

        return account;
    }
}
