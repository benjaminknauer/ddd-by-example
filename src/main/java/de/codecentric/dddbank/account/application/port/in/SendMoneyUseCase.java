package de.codecentric.dddbank.account.application.port.in;

import de.codecentric.dddbank.account.domain.model.shared.MonetaryAmount;
import de.codecentric.dddbank.account.domain.model.account.Account;
import de.codecentric.dddbank.account.domain.model.transaction.Transaction;

public interface SendMoneyUseCase {

    Transaction sendMoney(Account.AccountId from, Account.AccountId to, MonetaryAmount amount);

}
