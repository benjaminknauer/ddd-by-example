package de.codecentric.dddbank.account.application.service;

import de.codecentric.dddbank.account.application.port.out.PublishTransactionsPort;
import de.codecentric.dddbank.account.domain.model.transaction.TransactionCreated;
import lombok.RequiredArgsConstructor;
import org.jmolecules.ddd.annotation.Service;
import org.springframework.modulith.ApplicationModuleListener;

@Service
@RequiredArgsConstructor
public class TransactionEventListener {

    private final PublishTransactionsPort publishTransactionsPort;

    @ApplicationModuleListener
    public void handle(TransactionCreated transactionCreated) {
        throw new RuntimeException("Error in Kafka");
        //publishTransactionsPort.publishTransactionCreated(transactionCreated);
    }
}
