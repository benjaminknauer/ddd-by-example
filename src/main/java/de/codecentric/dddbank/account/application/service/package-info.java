@Application
@ApplicationServiceRing
package de.codecentric.dddbank.account.application.service;

import org.jmolecules.architecture.hexagonal.Application;
import org.jmolecules.architecture.onion.classical.ApplicationServiceRing;