package de.codecentric.dddbank.account.domain.service;

import de.codecentric.dddbank.account.domain.model.account.Account;
import de.codecentric.dddbank.account.domain.model.shared.MonetaryAmount;
import de.codecentric.dddbank.account.domain.model.transaction.Transaction;
import lombok.RequiredArgsConstructor;
import org.jmolecules.ddd.annotation.Service;

@Service
@RequiredArgsConstructor
public class SendMoneyDomainService {

    public Transaction sendMoney(Account from, Account to, MonetaryAmount amount) {
        var transaction = Transaction.create(
                from,
                to,
                amount
        );

        from.addSentTransaction(transaction);
        to.addReceivedTransaction(transaction);

        return transaction;
    }

}
