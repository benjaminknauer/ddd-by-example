package de.codecentric.dddbank.account.domain.model.account;


public class InsufficientFundsException extends RuntimeException {

    public InsufficientFundsException(String message) {
        super(message);
    }


}
