package de.codecentric.dddbank.account.domain.model.account;

import org.jmolecules.ddd.types.Repository;

import java.util.Optional;

public interface AccountRepository extends Repository<Account, Account.AccountId> {

    void save(Account account);

    Optional<Account> findById(Account.AccountId id);

}
