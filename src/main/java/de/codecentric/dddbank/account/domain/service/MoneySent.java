package de.codecentric.dddbank.account.domain.service;

import de.codecentric.dddbank.account.domain.model.transaction.Transaction;
import org.jmolecules.event.types.DomainEvent;

public record MoneySent(Transaction transaction) implements DomainEvent {
}
