package de.codecentric.dddbank.account.domain.model.shared;


public enum Currency {
    EUR, USD

}
