package de.codecentric.dddbank.account.domain.model.account;

import de.codecentric.dddbank.account.domain.model.shared.Currency;
import de.codecentric.dddbank.account.domain.model.shared.MonetaryAmount;
import de.codecentric.dddbank.account.domain.model.transaction.Transaction;
import lombok.*;
import org.jmolecules.ddd.types.AggregateRoot;
import org.jmolecules.ddd.types.Identifier;
import org.springframework.data.domain.AbstractAggregateRoot;

import java.util.UUID;

@AllArgsConstructor(staticName = "of")
@Getter
public class Account extends AbstractAggregateRoot<Account> implements AggregateRoot<Account, Account.AccountId> {

    private final AccountId id;
    private MonetaryAmount balance;

    public static Account create() {
        return Account.of(
                new AccountId(UUID.randomUUID()),
                MonetaryAmount.of(0, Currency.EUR)
        );
    }


    public void addSentTransaction(Transaction transaction) {
        boolean isFromAccountIdDoesNotMatch = !transaction.getFrom().getId().equals(id);
        if (isFromAccountIdDoesNotMatch) {
            throw new IllegalArgumentException("Provided from transactionId does not match");
        }
        boolean isNotEnoughMoney = transaction.getAmount().isGreaterThan(balance);
        if (isNotEnoughMoney) {
            throw new InsufficientFundsException("Not enogh Money");
        }

        this.balance = this.balance.subtract(transaction.getAmount());
    }

    public void addReceivedTransaction(Transaction transaction) {
        boolean isToAccountIdDoesNotMatch = !transaction.getTo().getId().equals(id);
        if (isToAccountIdDoesNotMatch) {
            throw new IllegalArgumentException("Provided to transactionId does not match");
        }

        this.balance = this.balance.add(transaction.getAmount());
    }

    public boolean hasBalanceGreaterThan(MonetaryAmount monetaryAmount) {
        return balance.isGreaterThan(monetaryAmount);

    }


    public record AccountId(UUID id) implements Identifier { }
}
