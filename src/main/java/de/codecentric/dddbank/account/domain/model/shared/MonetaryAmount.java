package de.codecentric.dddbank.account.domain.model.shared;

import de.codecentric.dddbank.account.domain.tacticalpattern.InternalField;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import org.jmolecules.ddd.types.ValueObject;

@Value
@Builder(access = AccessLevel.PRIVATE)
public class MonetaryAmount implements ValueObject {

    @InternalField
    int amount;

    @Enumerated(EnumType.STRING)
    @NonNull Currency currency;

    public static MonetaryAmount of(int anAmount, Currency aCurrency) {
        if (anAmount < 0) {
            throw new IllegalArgumentException("Amount must not be less than 0");
        }
        return builder()
                .amount(anAmount)
                .currency(aCurrency)
                .build();
    }

    public MonetaryAmount add(MonetaryAmount aMonetaryAmount) {
        assertSameCurrency(aMonetaryAmount);
        var newAmount = this.getAmount() + aMonetaryAmount.getAmount();
        return MonetaryAmount.of(newAmount, this.getCurrency());
    }

    public MonetaryAmount subtract(MonetaryAmount aMonetaryAmount) {
        assertSameCurrency(aMonetaryAmount);
        var newAmount = this.getAmount() - aMonetaryAmount.getAmount();
        return MonetaryAmount.of(newAmount, this.getCurrency());
    }

    public boolean isGreaterThan(MonetaryAmount aMonetaryAmount) {
        assertSameCurrency(aMonetaryAmount);

        return this.amount > aMonetaryAmount.getAmount();
    }

    private void assertSameCurrency(MonetaryAmount anAmmount) {
        if (anAmmount.getCurrency() != this.currency) {
            throw new IllegalArgumentException("Currency must be equal");
        }
    }

}
