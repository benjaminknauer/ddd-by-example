package de.codecentric.dddbank.account.domain.model.transaction;

import de.codecentric.dddbank.account.domain.model.account.Account;
import org.jmolecules.ddd.types.Repository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TransactionRepository extends Repository<Transaction, Transaction.TransactionId> {

    void save(Transaction account);

    Optional<Transaction> findById(Transaction.TransactionId id);

    @Query("SELECT t FROM Transaction t WHERE t.from = :accountId")
    List<Transaction> findFrom(Account.AccountId accountId);

    @Query("SELECT t FROM Transaction t WHERE t.to = :accountId")
    List<Transaction> findTo(Account.AccountId accountId);


    @Query("SELECT t FROM Transaction t WHERE t.from = :accountId OR t.to = :accountId")
    List<Transaction> findAssociated(Account.AccountId accountId);

    List<Transaction> findAll();

}
