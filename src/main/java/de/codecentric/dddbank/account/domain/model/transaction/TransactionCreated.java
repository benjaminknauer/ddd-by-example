package de.codecentric.dddbank.account.domain.model.transaction;

import org.jmolecules.event.types.DomainEvent;

public record TransactionCreated(Transaction transaction) implements DomainEvent {
}
