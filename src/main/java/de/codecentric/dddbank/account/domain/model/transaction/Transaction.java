package de.codecentric.dddbank.account.domain.model.transaction;

import de.codecentric.dddbank.account.domain.model.account.Account;
import de.codecentric.dddbank.account.domain.model.shared.MonetaryAmount;
import jakarta.persistence.Column;
import jakarta.persistence.Transient;
import lombok.*;
import org.jmolecules.ddd.types.AggregateRoot;
import org.jmolecules.ddd.types.Association;
import org.jmolecules.ddd.types.Identifier;
import org.jmolecules.event.types.DomainEvent;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.DomainEvents;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@AllArgsConstructor(staticName = "of")
public class Transaction implements AggregateRoot<Transaction, Transaction.TransactionId> {


    @Transient
    private final List<DomainEvent> domainEvents = new ArrayList<>();

    @Id
    private final TransactionId id;
    @Column(name = "from_account")
    private Association<Account, Account.AccountId> from;
    @Column(name = "to_account")
    private Association<Account, Account.AccountId> to;
    private final MonetaryAmount amount;

    public static Transaction create(Account from, Account to, MonetaryAmount amount) {
        var transaction = Transaction.of(
                new Transaction.TransactionId(UUID.randomUUID()),
                Association.forAggregate(from),
                Association.forAggregate(to),
                amount
        );
        transaction.domainEvents.add(new TransactionCreated(transaction));
        return transaction;
    }

    @DomainEvents
    List<DomainEvent> getDomainEvents() {
        return List.copyOf(domainEvents);
    }

    public record TransactionId(UUID id) implements Identifier {
    }

}
