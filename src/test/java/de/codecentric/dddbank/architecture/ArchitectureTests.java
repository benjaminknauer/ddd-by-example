package de.codecentric.dddbank.architecture;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.jmolecules.archunit.JMoleculesArchitectureRules;
import org.jmolecules.archunit.JMoleculesDddRules;
import org.jmolecules.ddd.types.AggregateRoot;
import org.jmolecules.ddd.types.Entity;
import org.jmolecules.ddd.types.ValueObject;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static de.codecentric.dddbank.architecture.OnlyExposeTheirInternalFieldsToAdapterLayers.onlyExposeTheirInternalFieldsToAdapterLayers;

@AnalyzeClasses(packages = "de.codecentric.dddbank..")
class ArchitectureTests {

    @ArchTest
    ArchRule ensureDddRules = JMoleculesDddRules.all();

    @ArchTest
    ArchRule ensureOnionRules = JMoleculesArchitectureRules.ensureOnionClassical();

    @ArchTest
    ArchRule ensureHexagonalRules = JMoleculesArchitectureRules.ensureHexagonal();

    @ArchTest
    ArchRule domainDoesNotExposeInternals = classes()
            .that().implement(AggregateRoot.class)
            .or().implement(Entity.class)
            .or().implement(ValueObject.class)
            .should(onlyExposeTheirInternalFieldsToAdapterLayers());

}
