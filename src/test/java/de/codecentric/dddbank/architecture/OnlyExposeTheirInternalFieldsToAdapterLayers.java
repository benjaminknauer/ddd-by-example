package de.codecentric.dddbank.architecture;

import com.tngtech.archunit.core.domain.*;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.SimpleConditionEvent;
import de.codecentric.dddbank.account.domain.tacticalpattern.InternalField;
import org.jmolecules.architecture.hexagonal.Adapter;

public class OnlyExposeTheirInternalFieldsToAdapterLayers extends ArchCondition<JavaClass> {

    private OnlyExposeTheirInternalFieldsToAdapterLayers() {
        super("only expose their internal fields to adapter layers");
    }

    public static OnlyExposeTheirInternalFieldsToAdapterLayers onlyExposeTheirInternalFieldsToAdapterLayers() {
        return new OnlyExposeTheirInternalFieldsToAdapterLayers();
    }

    @Override
    public void check(JavaClass javaClass, ConditionEvents conditionEvents) {
        for (JavaField field : javaClass.getFields()) {
            if (field.isAnnotatedWith(InternalField.class)) {
                checkFieldAccess(field, conditionEvents);
            }
        }
    }

    private void checkFieldAccess(JavaField field, ConditionEvents conditionEvents) {

        for (JavaFieldAccess fieldAccess : field.getAccessesToSelf()) {
            if (mightBeAccessFromGetterMethod(field, fieldAccess)) {
                checkGetterAccess(field, fieldAccess, conditionEvents);
            } else {
                checkNonGetterAccess(field, fieldAccess, conditionEvents);
            }
        }
    }

    private boolean mightBeAccessFromGetterMethod(JavaField field, JavaFieldAccess fieldAccess) {
        var fieldType = field.getType();
        var fieldDefinedInClass = field.getOwner();
        var fieldAccessCodeUnit = fieldAccess.getOwner();
        var fieldAccessClass = fieldAccessCodeUnit.getOwner();

        boolean isAccessFromMethod = fieldAccess.getOwner().isMethod();
        boolean isPrivateAccess = fieldAccessClass.equals(fieldDefinedInClass);
        boolean isReturnsSameTypeAsField = fieldAccess.getOwner().getReturnType().equals(fieldType);

        return isReturnsSameTypeAsField
                && isPrivateAccess
                && isAccessFromMethod
                && fieldAccess.getAccessType().equals(JavaFieldAccess.AccessType.GET);
    }

    private void checkGetterAccess(JavaField field, JavaFieldAccess fieldAccess, ConditionEvents conditionEvents) {
        var fieldAccessCodeUnit = fieldAccess.getOwner();
        var fieldClass = field.getOwner();
        var getterAccesses = fieldAccessCodeUnit.getAccessesToSelf();
        for (var getterAccess : getterAccesses) {
            var getterAccessClass = getterAccess.getOwner().getOwner();
            boolean isPrivateGetterAccess = getterAccessClass.equals(fieldClass);
            boolean isGetterAccessInAdapter = getterAccessClass.getPackage().isAnnotatedWith(Adapter.class);

            if (!isPrivateGetterAccess && !isGetterAccessInAdapter) {
                var message = String.format("Field '%s' should not be accessed through getter %s from '%s'",
                        field.getFullName(),
                        fieldAccessCodeUnit.getFullName(),
                        getterAccess.getOwner().getFullName());
                conditionEvents.add(SimpleConditionEvent.violated(field, message));
            }
        }
    }

    private void checkNonGetterAccess(JavaField field, JavaFieldAccess javaFieldAccess, ConditionEvents conditionEvents) {
        var fieldAccessClass = javaFieldAccess.getOwner().getOwner();
        boolean isAccessFromAdapter = fieldAccessClass.getPackage().isAnnotatedWith(Adapter.class);

        if (!fieldAccessClass.equals(field.getOwner()) && !isAccessFromAdapter) {
            var message = String.format("Field '%s' should not be accessed from '%s'", field.getFullName(), fieldAccessClass.getFullName());
            conditionEvents.add(SimpleConditionEvent.violated(field, message));
        }
    }
}
