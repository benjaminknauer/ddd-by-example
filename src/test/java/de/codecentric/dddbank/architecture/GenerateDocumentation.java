package de.codecentric.dddbank.architecture;

import de.codecentric.dddbank.DddBankApplication;
import org.jmolecules.architecture.hexagonal.Application;
import org.junit.jupiter.api.Test;
import org.springframework.modulith.ApplicationModule;
import org.springframework.modulith.core.ApplicationModules;
import org.springframework.modulith.docs.Documenter;

import java.awt.*;

public class GenerateDocumentation {
    @Test
    void generateDocumentation() {

        var diagramOptions = Documenter.DiagramOptions.defaults()
                .withDependencyTypes()
                .withStyle(Documenter.DiagramOptions.DiagramStyle.UML)
                .withStyle(Documenter.DiagramOptions.DiagramStyle.C4);
        var canvasOptions = Documenter.CanvasOptions.defaults();

        new Documenter(ApplicationModules.of(DddBankApplication.class)).writeDocumentation(diagramOptions, canvasOptions);
    }
}
